$(document).ready(function(){
    // portfolio masonry grid
    $(".filter-button").click(function(){
        $(".filter-button").removeClass("active")
        $(this).addClass("active")
        console.log('clicked')
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            $('.filter').show('1000');
        }
        else
        {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
 
    // testimonials slider
     var owl = $('.owl-carousel');
      owl.owlCarousel({
        items: 2,
        loop: true,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1
            },
            1000:{
                items:2
            }
        }
      });


    // animations
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $(window).on('resize scroll', function() {
        if ($('.text__Anime').isInViewport()) {
            console.log('is there')
            // do something
            $('.text__Anime').addClass('animated fadeInLeft ad1')
        } else {
            // do something else
            $('.text__Anime').removeClass('animated fadeInLeft ad1')
        }
    });
});